//  Created by Simran Preet Narang on 10/6/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        let stocksViewController = StocksListViewController()
        let navigationController = UINavigationController(rootViewController: stocksViewController)
        navigationController.navigationBar.prefersLargeTitles = true
        
        guard let scene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: scene)
        window?.makeKeyAndVisible()
        window?.rootViewController = navigationController
    }
}

