//  Created by Simran Preet Narang on 10/6/21.
//

import Foundation

struct Constants {
    struct Endpoints {
        static let portfolio = "https://storage.googleapis.com/cash-homework/cash-stocks-api/portfolio.json"
        static let portfolioEmpty = "https://storage.googleapis.com/cash-homework/cash-stocks-api/portfolio_empty.json"
        static let portfolioMalformed = "https://storage.googleapis.com/cash-homework/cash-stocks-api/portfolio_malformed.json"
    }
}
