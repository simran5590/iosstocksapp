//  Created by Simran Preet Narang on 10/6/21.
//

import Foundation

struct PortfolioAPIResponse: Codable {
    let stocks: [Stock]
}
