//  Created by Simran Preet Narang on 10/6/21.
//

import Foundation

struct Stock: Codable {
    let ticker: String
    let name: String
    let currency: String
    let currentPriceCents: Int
    let quantity: Int?
    let currentPriceTimestamp: Int
}
