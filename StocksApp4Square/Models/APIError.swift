//


import Foundation

enum APIError: Error {
    var localizedDescription: String {
        switch self {
        case .invalidURL:
            return "Invalid URL, please check you are calling a valid and correct endpoint."
        case .failedToDecode:
            return "Failed to convert API response."
        case .somethingWentWrong:
            return "Something went wrong. please try again."
        }
    }
    
    case invalidURL
    case failedToDecode
    case somethingWentWrong
}
