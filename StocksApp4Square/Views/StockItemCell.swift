//  Created by Simran Preet Narang on 10/6/21.
//

import UIKit

class StockItemCell: UITableViewCell {
    private let ticketLabel = UILabel.makeLabel(withSize: 24, weight: .bold)
    private let nameLabel = UILabel.makeLabel(withSize: 20, weight: .bold)
    private let currencyLabel = UILabel.makeLabel(withSize: 20, weight: .heavy)
    private let currentPriceLabel = UILabel.makeLabel(withSize: 16)
    private let quantityLabel = UILabel.makeLabel(withSize: 16)
    private let totalPriceLabel = UILabel.makeLabel(withSize: 16)
    private let lastUpdatedLabel = UILabel.makeLabel(withSize: 12, color: .systemGray)
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateCell(withStockViewModel stock: StockViewModel) {
        ticketLabel.text = stock.ticker
        nameLabel.text = stock.name
        currencyLabel.text = "(\(stock.currency))"
        currentPriceLabel.text = "Price:  $\(String(format: "%.2f", stock.currentPriceInDollars))"
        totalPriceLabel.text = "Total: $\(String(format: "%.2f", stock.totalPrice))"
        quantityLabel.text = "Quantity: \(stock.quantity)"
        lastUpdatedLabel.text = "Last Update: \(stock.lastUpdatedFormattedDate)"
    }
    
    private func setupUI() {
        let priceQuantitySV = UIStackView(arrangedSubviews: [currentPriceLabel, quantityLabel])
        priceQuantitySV.distribution = .fillEqually
        priceQuantitySV.axis = .horizontal
        priceQuantitySV.spacing = 4
        
        let ticketCurrency = UIStackView(arrangedSubviews: [ticketLabel, currencyLabel])
        ticketCurrency.distribution = .fillProportionally
        ticketCurrency.axis = .horizontal
        ticketCurrency.alignment = .center
        ticketCurrency.spacing = 4
        
        let mainStackView = UIStackView(arrangedSubviews: [ticketCurrency, nameLabel, priceQuantitySV, totalPriceLabel, lastUpdatedLabel])
        mainStackView.distribution = .fillEqually
        mainStackView.axis = .vertical
        mainStackView.alignment = .leading
        mainStackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(mainStackView)
        NSLayoutConstraint.activate([
            mainStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            mainStackView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            mainStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            mainStackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
        ])
    }
}
