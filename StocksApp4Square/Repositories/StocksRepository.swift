//  Created by Simran Preet Narang on 10/7/21.
//

import Foundation

protocol StocksRepositoryInterface {
    func loadStocks(endpoint: String, completion:@escaping (Result<[Stock], APIError>) -> Void) throws
}

class StocksRepository: StocksRepositoryInterface {
    private let networkinManager: NetworkingServiceProtocol
    
    init(withNetworkinManager networkingManager: NetworkingServiceProtocol = NetworkingService(session: URLSession.shared)) {
        self.networkinManager = networkingManager
    }
    
    func loadStocks(endpoint: String, completion:@escaping (Result<[Stock], APIError>) -> Void) throws {
            try networkinManager.GET(type: PortfolioAPIResponse.self,
                                     urlString: endpoint) { result in
                switch(result) {
                case .success(let response):
                    completion(.success(response.stocks))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
    }
}
