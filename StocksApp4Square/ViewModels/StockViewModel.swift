//  Created by Simran Preet Narang on 10/6/21.
//

import Foundation

final class StockViewModel {
    private(set) var stock: Stock
    
    init(withStock stock: Stock) {
        self.stock = stock
    }
    
    var ticker: String {
        return stock.ticker
    }
    var name: String {
        return stock.name
    }
    var currency: String {
        return stock.currency
    }
    var currentPriceCents: Int {
        return stock.currentPriceCents
    }
    var currentPriceInDollars: Double {
        return Double(stock.currentPriceCents)/100.0
    }
    var quantity: Int {
        return stock.quantity ?? 0
    }
    var totalPrice: Double {
        return currentPriceInDollars * Double(quantity)
    }
    var lastUpdatedDate: Date {
        let currentDate = Date(timeIntervalSince1970: TimeInterval(stock.currentPriceTimestamp))
        return currentDate
    }
    var lastUpdatedFormattedDate: String {
        return lastUpdatedDate.formatted(date: .abbreviated, time: .standard)
    }
    var lastUpdatedTimeStamp: Int {
        return stock.currentPriceTimestamp
    }
}
