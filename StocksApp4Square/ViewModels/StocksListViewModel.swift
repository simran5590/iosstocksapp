//  Created by Simran Preet Narang on 10/6/21.
//

import Foundation

protocol StocksListViewModelDelegate: AnyObject {
    func stockDidLoadSuccessfully()
    func stocksDidLoad(withError error: Error)
    func stocksLoadedFromCache(withError error: Error?)
    func updateWithSearchResults()
}

protocol StocksListViewModelInterface {
    var stocks: [StockViewModel] { get }
    var filteredStocks: [StockViewModel] { get }
    var delegate: StocksListViewModelDelegate? { get set }
    var portfolioEndpoint: String { get set }
    var searchText: String { get set }
    var isSearching: Bool { get set }
    
    func loadStocks()
}

final class StocksListViewModel: StocksListViewModelInterface {
    
    private var cachedStocksKey = "CachedStocks"

    private var repository: StocksRepositoryInterface
    
    private var _stocks: [StockViewModel] = []
    
    var filteredStocks: [StockViewModel] = []
    
    var stocks: [StockViewModel] {
        if _stocks.count == 0 {
            return fetchCachedStocks().map{ StockViewModel(withStock: $0) }
        }
        return _stocks
    }
    
    var searchText: String = "" {
        didSet {
            isSearching = !searchText.isEmpty
            let lowercasedSearchText = searchText.lowercased()
            let containsMatchFilteredStocks = stocks.filter {
                $0.name.lowercased().contains(lowercasedSearchText)
                || $0.ticker.lowercased().contains(lowercasedSearchText)
            }
            let exactMatchFilteredStocks = containsMatchFilteredStocks.filter {
                $0.name.lowercased() == lowercasedSearchText
                || $0.ticker.lowercased() == lowercasedSearchText
            }
            let prefixMatchFilteredStocks = containsMatchFilteredStocks.filter {
                $0.name.lowercased().hasPrefix(lowercasedSearchText)
                || $0.ticker.lowercased().hasPrefix(lowercasedSearchText)
            }
            
            
            filteredStocks = stocks.filter {
                $0.name.lowercased() == lowercasedSearchText
                || $0.ticker.lowercased() == lowercasedSearchText
            }
             delegate?.updateWithSearchResults()
        }
    }
    
    var isSearching: Bool = false
    
    
    public weak var delegate: StocksListViewModelDelegate?
    
    
    public var portfolioEndpoint: String = Constants.Endpoints.portfolio {
        didSet {
            loadStocks()
        }
    }
    
    init(repository: StocksRepositoryInterface = StocksRepository()) {
        self.repository = repository
    }
    
    func loadStocks() {
        do {
            try repository.loadStocks(endpoint: portfolioEndpoint, completion: { [weak self] result in
                switch(result) {
                case .success(let stocks):
                    self?.cacheStocks(stocks: stocks)
                    self?._stocks = stocks.map{ StockViewModel(withStock: $0) }
                    self?.delegate?.stockDidLoadSuccessfully()
                case .failure(let error):
                    if self?.fetchCachedStocks().count ?? 0 > 0 {
                        self?.delegate?.stocksLoadedFromCache(withError: error)
                    } else {
                        self?.delegate?.stocksDidLoad(withError: error)
                    }
                }
            })
        } catch let error {
            delegate?.stocksDidLoad(withError: error)
        }
    }
    
    private func cacheStocks(stocks: [Stock]) {
        guard let stocksData =  try? JSONEncoder().encode(stocks) else {
            // TODO: Handle the failure
            return
        }
        UserDefaults.standard.set(stocksData, forKey: cachedStocksKey)
    }
    
    private func fetchCachedStocks() -> [Stock] {
        guard let stocksData = UserDefaults.standard.data(forKey: cachedStocksKey) else {
            // TODO: Handle the failure
            return []
        }
        let stocks = try? JSONDecoder().decode([Stock].self, from: stocksData)
        return stocks ?? []
    }
    
}

