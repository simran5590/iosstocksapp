//
//  UIViewController+Extensions.swift
//  StocksApp4Square
//
//  Created by Simran Preet Narang on 10/7/21.
//

import Foundation
import UIKit

extension UIViewController {
    func showPlaceHolderView(withMessage message: String) {
        refreshEmptyViewState()
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.accessibilityIdentifier = "PlaceholderView"
        label.text = message
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .systemGray
        
        view.addSubview(label)
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    func showLoading() {
        refreshEmptyViewState()
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.startAnimating()
        
        view.addSubview(activityIndicator)
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
    
    func hideLoading() {
        guard let activityIndicateView = view.subviews.first(where: { $0 is UIActivityIndicatorView }) else {
            return
        }
        activityIndicateView.removeFromSuperview()
        refreshEmptyViewState()
    }
    
    func showAlert(withTitle title: String, message: String, okAction: (() -> Void)? = nil, cancelAction: (() -> Void)? = nil) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { alertAction in
            okAction?()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { alertAction in
            cancelAction?()
        }
        ac.addAction(okAction)
        ac.addAction(cancelAction)
        present(ac, animated: true)
    }
    
    private func refreshEmptyViewState() {
        if let emptyStateLabel = view.subviews.first(where: { $0.accessibilityIdentifier == "PlaceholderView" }) {
            emptyStateLabel.removeFromSuperview()
        }
    }
}
