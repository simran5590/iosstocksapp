//  Created by Simran Preet Narang on 10/7/21.
//

import Foundation
import UIKit

extension UILabel {
    static func makeLabel(withSize size: CGFloat, weight: UIFont.Weight = .regular, color: UIColor = .label)-> UILabel {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: size, weight: weight)
        label.textColor = color
        return label
    }
}
