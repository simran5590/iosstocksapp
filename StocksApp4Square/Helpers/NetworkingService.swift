//  Created by Simran Preet Narang on 10/6/21.
//

import Foundation

protocol NetworkingServiceProtocol {
    func GET<T: Codable>(type: T.Type,
                         urlString: String,
                         completion: @escaping NetworkRequestCompletionHander<T>) throws
}

typealias NetworkRequestCompletionHander<T> = ((Result<T, APIError>) -> Void)

class NetworkingService: NetworkingServiceProtocol {
    private let session: URLSession
    
    init(session: URLSession) {
        self.session = session
    }
    
    func GET<T: Codable>(type: T.Type,
                         urlString: String,
                         completion: @escaping NetworkRequestCompletionHander<T>) throws {
        guard let url = URL(string: urlString) else {
            throw APIError.invalidURL
        }
        
        let request = URLRequest(url: url)
        
        session.dataTask(with: request) { [weak self] data, response, error in
            guard error == nil,
                  let data = data else {
                      self?.handError(type: type, error: error, completion: completion)
                      return
                  }
            self?.handleResponse(type: type, response: response, data: data, completion: completion)
        }.resume()
    }
}


extension NetworkingService {

    private func handleResponse<T: Codable>(type: T.Type,
                                            response: URLResponse?,
                                            data: Data,
                                            completion: @escaping NetworkRequestCompletionHander<T>) {
        
        if let httpResponse = response as? HTTPURLResponse,
           httpResponse.statusCode == 200 {
            do {
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let decodedResponse = try decoder.decode(type, from: data)
                completion(.success(decodedResponse))
            } catch {
                print("\(#function) in \(#file) failed to decode with error: \(error)")
                completion(.failure(.failedToDecode))
            }
        } else {
            completion(.failure(.somethingWentWrong))
        }
    }
    
    private func handError<T: Codable>(type: T.Type,
                                       error: Error?,
                                       completion: @escaping NetworkRequestCompletionHander<T>) {
        print("\(#function) in \(#file) failed with error: \(String(describing: error?.localizedDescription))")
        completion(.failure(.somethingWentWrong))
    }
}
