//  Created by Simran Preet Narang on 10/6/21.
//

import UIKit

class StocksListViewController: UIViewController {
    
    private let stockItemCellIdentifier = "\(StockItemCell.self)"
    private var viewModel: StocksListViewModelInterface
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.isHidden = true
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(StockItemCell.self, forCellReuseIdentifier: stockItemCellIdentifier)
        return tableView
    }()
    
    
    var searchController: UISearchController = {
        let sc = UISearchController()
        return sc
    }()
    
    init(withViewModel viewModel: StocksListViewModelInterface = StocksListViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupTableView()
        setupViewModel()
        setupNavigationItem()
    }
}


extension StocksListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.isSearching ? viewModel.filteredStocks.count : viewModel.stocks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: stockItemCellIdentifier, for: indexPath) as! StockItemCell
        let stock = viewModel.isSearching ? viewModel.filteredStocks[indexPath.row] : viewModel.stocks[indexPath.row]
        cell.updateCell(withStockViewModel: stock)
        return cell
    }
}


extension StocksListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text else {
            return
        }
        viewModel.searchText = searchText
    }
}


extension StocksListViewController: StocksListViewModelDelegate {
    func stockDidLoadSuccessfully() {
        DispatchQueue.main.async { [weak self] in
            self?.hideLoading()
            if self?.viewModel.stocks.isEmpty ?? true {
                self?.tableView.isHidden = true
                self?.showPlaceHolderView(withMessage: "Your portfolio is empty.\n 🙁")
                return
            }
            
            self?.tableView.isHidden = false
            self?.tableView.reloadData()
        }
    }
    
    func stocksDidLoad(withError error: Error) {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.isHidden = true
            self?.hideLoading()
            print("APIERROR: \(error.localizedDescription)")
            self?.showAlert(
                withTitle: "Something went wrong.",
                message: "There seems to be an issue on our side. We are trying to do our best to fix it. Tap OK to refresh.",
                okAction: {
                    self?.showLoading()
                    self?.viewModel.portfolioEndpoint = Constants.Endpoints.portfolio
                },
                cancelAction: {
                    self?.showPlaceHolderView(withMessage: "Sorry for the inconvenience")
                })
        }
    }
    
    func stocksLoadedFromCache(withError error: Error?) {
        DispatchQueue.main.async { [weak self] in
            self?.hideLoading()
            if self?.viewModel.stocks.isEmpty ?? true {
                self?.tableView.isHidden = true
                self?.showPlaceHolderView(withMessage: "Your portfolio is empty.\n 🙁")
                return
            }
            
            self?.tableView.isHidden = false
            self?.tableView.reloadData()
            
            if let error = error {
                print("ERROR: In \(#function) : \(error.localizedDescription)")
                self?.showAlert(withTitle: "Failed to load stocks",
                                message: "", okAction: {
                    
                }, cancelAction: nil)
            }
        }
    }
    
    func updateWithSearchResults() {
        DispatchQueue.main.async { [weak self] in
            if self?.viewModel.filteredStocks.count == 0 {
                self?.tableView.isHidden = true
                self?.showPlaceHolderView(withMessage: "No results founds")
                return
            }
            self?.tableView.isHidden = false
            self?.tableView.reloadData()
            self?.showPlaceHolderView(withMessage: "")
        }
    }
}



// MARK:- Private methods
private extension StocksListViewController {
    func setupUI() {
        view.backgroundColor = .systemBackground
        title = "Stocks"
        
        // SearchController setup
        navigationItem.searchController = searchController
        searchController.searchResultsUpdater = self
    }
    
    func setupViewModel() {
        viewModel.delegate = self
        viewModel.loadStocks()
        showLoading()
    }
    
    func setupTableView() {
        view.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.readableContentGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.readableContentGuide.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.readableContentGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.readableContentGuide.bottomAnchor)
        ])
        tableView.dataSource = self
    }
    
    func setupNavigationItem() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "ellipsis.circle"), style: .done, target: self, action: #selector(openStateOptions))
    }
    
    @objc func openStateOptions() {
        let ac = UIAlertController(title: "Choose your state", message: "", preferredStyle: .actionSheet)
        let portfolioEndpointAction = UIAlertAction(title: "Portfolio Endpoint", style: .default) { [weak self] alertAction in
            self?.tableView.isHidden = true
            self?.viewModel.portfolioEndpoint = Constants.Endpoints.portfolio
            self?.showLoading()
        }
        
        let portfolioEmptyEndpointAction = UIAlertAction(title: "Portfolio Empty Endpoint", style: .default) { [weak self] alertAction in
            self?.tableView.isHidden = true
            self?.viewModel.portfolioEndpoint = Constants.Endpoints.portfolioEmpty
            self?.showLoading()
        }
        
        let portfolioMalformedEndpointAction = UIAlertAction(title: "Portfolio Malformed Endpoint", style: .default) { [weak self] alertAction in
            self?.tableView.isHidden = true
            self?.viewModel.portfolioEndpoint = Constants.Endpoints.portfolioMalformed
            self?.showLoading()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        ac.addAction(portfolioEndpointAction)
        ac.addAction(portfolioEmptyEndpointAction)
        ac.addAction(portfolioMalformedEndpointAction)
        ac.addAction(cancelAction)
        present(ac, animated: true)
    }
}
