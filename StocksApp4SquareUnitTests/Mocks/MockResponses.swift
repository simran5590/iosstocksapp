//  Created by Simran Preet Narang on 10/7/21.
//

import Foundation
@testable import StocksApp4Square

struct MockResponses {
    static let successStocksApiResponse = """
{
    "stocks":[
        {"ticker":"TWTR","name":"Twitter, Inc.","currency":"USD","current_price_cents":3833,"quantity":1,"current_price_timestamp":1597942580},
        {"ticker":"^GSPC","name":"S&P 500","currency":"USD","current_price_cents":318157,"quantity":null,"current_price_timestamp":1597942580},
        {"ticker":"BAC","name":"Bofa Corporation","currency":"USD","current_price_cents":2393,"quantity":10,"current_price_timestamp":1597942580}
    ]
}
"""
    
    static let emptyStocksAPIResponse = """
{
    "stocks":[]
}
"""
    
    static let malformedStocksApiResponse = """
{
    "stocks":[
        {"ticker":"TWTR","name":"Twitter, Inc.","currency":"USD","current_price_cents":3833,"quantity":1,"current_price_timestamp":1597942580},
        {"ticker":"^GSPC","name":"S&P 500","currency":"USD","current_price_cents":318157,"quantity":null,"current_price_timestamp":1597942580},
        {"ticker":"BAC","name":"Bofa Corporation","currency":"USD","current_price_cents":2393,"quantity":10,"current_price_timestamp":1597942580}dslfknsdufbsiubuifbfiabpauifb
    ]
}
"""
    
    static var mockStocks: [Stock] {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        return try! jsonDecoder.decode(PortfolioAPIResponse.self, from: MockResponses.successStocksApiResponse.data(using: .utf8) ?? Data()).stocks
    }
    
    static var mockEmptyStocks: [Stock] {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        return try! jsonDecoder.decode(PortfolioAPIResponse.self, from: MockResponses.emptyStocksAPIResponse.data(using: .utf8) ?? Data()).stocks
    }
}
