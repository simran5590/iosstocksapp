//  Created by Simran Preet Narang on 10/7/21.
//

import XCTest
@testable import StocksApp4Square

class StocksRepositoryUnitTests: XCTestCase {

    var sut: StocksRepositoryInterface?
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let networkingManager = MockNetworkingManager()
        sut = StocksRepository(withNetworkinManager: networkingManager)
        
        MockURLProtocol.stubResponseData = nil
        MockURLProtocol.error = nil
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        MockURLProtocol.stubResponseData = nil
        MockURLProtocol.error = nil
    }

    func testStocksRepository_ForPortfolioEndpoint_ReturnsResponseWith3Stocks() throws {
        do {
            // Arrange
            MockURLProtocol.stubResponseData = MockResponses.successStocksApiResponse.data(using: .utf8)
            
            let expectation = self.expectation(description: "loadStocks() expectation")
            
            // Act
            try sut?.loadStocks(endpoint: Constants.Endpoints.portfolio, completion: { result in
                switch(result) {
                // Assert
                case .success(let response):
                    XCTAssertTrue(response.count == 3, "\(#function) failed. API did not returned 3 stocks")
                    
                case .failure(let error):
                    XCTFail("Expected \(#function) to pass, but failed with error \(error.localizedDescription)")
                }
                expectation.fulfill()
            })
            self.wait(for: [expectation], timeout: 5)
        } catch let error {
            XCTFail("\(#function) failed with error \(error.localizedDescription)")
        }
    }
    
    func testStocksRepository_ForPortfolioEmptyEndpoint_ReturnsResponseWithEmptyList() throws {
        do {
            // Arrange
            MockURLProtocol.stubResponseData = MockResponses.emptyStocksAPIResponse.data(using: .utf8)
            
            let expectation = self.expectation(description: "loadStocks() expectation")
            
            // Act
            try sut?.loadStocks(endpoint: Constants.Endpoints.portfolioEmpty, completion: { result in
                switch(result) {
                
                // Assert
                case .success(let response):
                    XCTAssertTrue(response.count == 0, "\(#function) failed. API did not returned 0 stocks")
                case .failure(let error):
                    XCTFail("Expected \(#function) to pass, but failed with error \(error.localizedDescription)")
                }
                expectation.fulfill()
            })
            self.wait(for: [expectation], timeout: 5)
        } catch let error {
            XCTFail("\(#function) failed with error \(error.localizedDescription)")
        }
    }
    
    func testStocksRepository_ForPortfolioMalformedEndpoint_ReturnsError() throws {
        do {
            // Arrange
            MockURLProtocol.stubResponseData = MockResponses.malformedStocksApiResponse.data(using: .utf8)
            
            let expectation = self.expectation(description: "loadStocks() expectation")
            
            // Act
            try sut?.loadStocks(endpoint: Constants.Endpoints.portfolioMalformed, completion: { result in
                switch(result) {
                
                // Assert
                case .success(_):
                    XCTFail("Should return at error in result, did not returned at error")
                    
                case .failure(let error):
                    XCTAssertTrue(error.localizedDescription == "Failed to convert API response.")
                }
                expectation.fulfill()
            })
            self.wait(for: [expectation], timeout: 5)
        } catch let error {
            XCTFail("\(#function) failed with error \(error.localizedDescription)")
        }
    }
}


fileprivate class MockNetworkingManager: NetworkingServiceProtocol {
    
    func GET<T>(type: T.Type, urlString: String, completion: @escaping NetworkRequestCompletionHander<T>) throws where T : Decodable, T : Encodable {
        
        guard let _ = URL(string: urlString) else {
            completion(.failure(.invalidURL))
            return
        }
        
        
        if urlString == Constants.Endpoints.portfolio {
            completion(.success(PortfolioAPIResponse(stocks: MockResponses.mockStocks) as! T))
        } else if urlString == Constants.Endpoints.portfolioEmpty {
            completion(.success(PortfolioAPIResponse(stocks: MockResponses.mockEmptyStocks) as! T))
        } else if urlString == Constants.Endpoints.portfolioMalformed {
            completion(.failure(.failedToDecode))
        } else {
            completion(.failure(.somethingWentWrong))
        }
    }
}
