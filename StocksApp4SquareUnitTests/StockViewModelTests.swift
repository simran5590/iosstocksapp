//


import Foundation
//  Created by Simran Preet Narang on 10/7/21.
//

import XCTest
@testable import StocksApp4Square

class StockViewModelUnitTests: XCTestCase {

    var sut: StockViewModel?
    
    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func testPortfolioEndpoint_ThatReturns3Stocks() throws {
        // Arrange
        let stock = Stock(ticker: "TWTR",
                          name: "Twitter",
                          currency: "USD",
                          currentPriceCents: 3833,
                          quantity: 1,
                          currentPriceTimestamp: 1597942580)
        // Act
        sut = StockViewModel(withStock: stock)
        
        // Assert
        XCTAssert(sut?.ticker == stock.ticker)
        XCTAssert(sut?.name == stock.name)
        XCTAssert(sut?.currency == stock.currency)
        XCTAssert(sut?.currentPriceCents == stock.currentPriceCents)
        XCTAssert(sut?.currentPriceInDollars == Double(stock.currentPriceCents) / 100.0)
        XCTAssert(sut?.totalPrice == (Double(stock.currentPriceCents) / 100.0) * Double(stock.quantity ?? 0) )
        XCTAssert(sut?.currency == stock.currency)
        XCTAssert(sut?.quantity == stock.quantity)
        XCTAssert(sut?.lastUpdatedTimeStamp == stock.currentPriceTimestamp)
        XCTAssert(sut?.lastUpdatedDate == Date(timeIntervalSince1970: TimeInterval(stock.currentPriceTimestamp)))
        XCTAssert(sut?.lastUpdatedFormattedDate == Date(timeIntervalSince1970: TimeInterval(stock.currentPriceTimestamp)).formatted(date: .abbreviated, time: .standard))
    }
}
