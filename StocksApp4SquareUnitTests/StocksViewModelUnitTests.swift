//  Created by Simran Preet Narang on 10/7/21.
//

import XCTest
@testable import StocksApp4Square

class StocksViewModelUnitTests: XCTestCase {

    var sut: StocksListViewModelInterface?
    var mockRespository: MockStockRepository?
    
    override func setUpWithError() throws {
        mockRespository = MockStockRepository()
        sut = StocksListViewModel(repository: mockRespository!)
    }

    override func tearDownWithError() throws {
        
    }

    func testPortfolioEndpoint_ThatReturns3Stocks() throws {
        // Arrange
        let expectation = self.expectation(description: "loadStocks() expectation")
        sut?.portfolioEndpoint = Constants.Endpoints.portfolio
        
        // Act
        sut?.loadStocks()
        
        // Assert
        XCTAssertTrue(sut?.stocks.count == 3, "Should be 3, but it is not 3")
        XCTAssertNil(mockRespository?.error, "Error should be nil, but it is not nil")
        expectation.fulfill()
        self.wait(for: [expectation], timeout: 2)
    }
    
    func testPortfolioEmptyEndpoint_ThatReturns0Stocks() throws {
        // Arrange
        let expectation = self.expectation(description: "loadStocks() expectation")
        
        sut?.portfolioEndpoint = Constants.Endpoints.portfolioEmpty
        
        // Act
        sut?.loadStocks()
        
        // Assert
        XCTAssertTrue(sut?.stocks.count == 0, "Should be 0, but it is not 0")
        XCTAssertNil(mockRespository?.error, "Error should be nil, but it is not nil")
        expectation.fulfill()
        self.wait(for: [expectation], timeout: 2)
    }
    
    func testPortfolioEndpoint_ThatReturnsAnError() throws {
        // Arrange
        let expectation = self.expectation(description: "loadStocks() expectation")
        
        sut?.portfolioEndpoint = Constants.Endpoints.portfolioMalformed
        
        // Act
        sut?.loadStocks()
        
        // Assert
        XCTAssertNotNil(mockRespository?.error, "Error should be not nil, but it is nil.")
        XCTAssert(mockRespository?.error == .failedToDecode, "Error should be to '.failedToDevode` error, but it is: \(String(describing: mockRespository?.error?.localizedDescription))")
        expectation.fulfill()
        self.wait(for: [expectation], timeout: 2)
    }
    
    func testPortfolioWrongEndpoint_ThatReturnsAnError() throws {
        // Arrange
        let expectation = self.expectation(description: "loadStocks() expectation")
        sut?.portfolioEndpoint = "Invalid URL"
        
        // Act
        sut?.loadStocks()
        
        // Assert
        XCTAssertNotNil(mockRespository?.error, "Error should be not nil, but it is nil.")
        XCTAssert(mockRespository?.error == .invalidURL, "Error should be to '.invalidUrl` error, but it is: \(String(describing: mockRespository?.error?.localizedDescription))")
        expectation.fulfill()
        self.wait(for: [expectation], timeout: 2)
    }
    
    func testPortfolioEndpoint_ThatReturnsCachedStocksWhenNetworkFails() throws {
        // Arrange
        let expectation = self.expectation(description: "loadStocks() expectation")
        sut?.portfolioEndpoint = Constants.Endpoints.portfolio
        
        // Act
        sut?.loadStocks()
        sut?.portfolioEndpoint = Constants.Endpoints.portfolioMalformed
        
        // Assert
        XCTAssertTrue(sut?.stocks.count == 3, "Should be 3, but it is not 3")
        expectation.fulfill()
        self.wait(for: [expectation], timeout: 2)
    }
    
    func testPortfolioEndpoint_SearchingWithTickerNameResultsInExactStocksBeingReturned() throws {
        // Arrange
        let expectation = self.expectation(description: "loadStocks() expectation")
        sut?.portfolioEndpoint = Constants.Endpoints.portfolio
        
        // Act
        sut?.loadStocks()
        sut?.searchText = "TWTR"
        
        // Assert
        XCTAssertTrue(sut?.isSearching ?? false, "When search text is not empty, `isSearching` should be true, but it is not.")
        XCTAssertTrue(sut?.filteredStocks.count == 1, "Should be 1, but it is not 1")
        XCTAssertTrue(sut?.stocks.count == 3, "Non filtered stocks should remain 3")
        expectation.fulfill()
        self.wait(for: [expectation], timeout: 2)
    }
    
    
    func testPortfolioEndpoint_SearchingWithCompanyNameResultsInExactStocksBeingReturned() throws {
        // Arrange
        let expectation = self.expectation(description: "loadStocks() expectation")
        sut?.portfolioEndpoint = Constants.Endpoints.portfolio
        
        // Act
        sut?.loadStocks()
        sut?.searchText = "Twitter, Inc."
        
        // Assert
        XCTAssertTrue(sut?.isSearching ?? false, "When search text is not empty, `isSearching` should be true, but it is not.")
        XCTAssertTrue(sut?.filteredStocks.count == 1, "Should be 1, but it is not 1")
        XCTAssertTrue(sut?.stocks.count == 3, "Non filtered stocks should remain 3")
        expectation.fulfill()
        self.wait(for: [expectation], timeout: 2)
    }
    
    func testPortfolioEndpoint_SearchingWithEmptySearchTextResultsInZeroStocksBeingFiltered() throws {
        // Arrange
        let expectation = self.expectation(description: "loadStocks() expectation")
        sut?.portfolioEndpoint = Constants.Endpoints.portfolio
        
        // Act
        sut?.loadStocks()
        sut?.searchText = ""
        
        // Assert
        XCTAssertFalse(sut?.isSearching ?? true, "When search text is empty, `isSearching` should be FALSE, but it is not.")
        XCTAssertTrue(sut?.filteredStocks.count == 0, "Should be 0, but it is not 0")
        XCTAssertTrue(sut?.stocks.count == 3, "Non filtered stocks should remain 3")
        expectation.fulfill()
        self.wait(for: [expectation], timeout: 2)
    }
    
    func testPortfolioEndpoint_SearchingWithNonExistentStockSearchTextResultsInZeroStocksBeingFiltered() throws {
        // Arrange
        let expectation = self.expectation(description: "loadStocks() expectation")
        sut?.portfolioEndpoint = Constants.Endpoints.portfolio
        
        // Act
        sut?.loadStocks()
        sut?.searchText = "Twitter/Square"
        
        // Assert
        XCTAssertTrue(sut?.isSearching ?? false, "When search text is non empty wront search, `isSearching` should be TRUE, but it is not.")
        XCTAssertTrue(sut?.filteredStocks.count == 0, "Should be 0, but it is not 0")
        XCTAssertTrue(sut?.stocks.count == 3, "Non filtered stocks should remain 3")
        expectation.fulfill()
        self.wait(for: [expectation], timeout: 2)
    }
    
    
}


class MockStockRepository: StocksRepositoryInterface {
    
    var error: APIError?
    
    func loadStocks(endpoint: String, completion: @escaping (Result<[Stock], APIError>) -> Void) throws {
        switch(endpoint) {
        case Constants.Endpoints.portfolio:
            completion(.success(MockResponses.mockStocks))
            error = nil
        case Constants.Endpoints.portfolioEmpty:
            completion(.success([]))
            error = nil
        case Constants.Endpoints.portfolioMalformed:
            completion(.failure(.failedToDecode))
            error = .failedToDecode
        default:
            completion(.failure(.invalidURL))
            error = .invalidURL
        }
    }
}

enum MockEndpoints: String {
    case wrongUrl = "This is a invalid URL."
}
