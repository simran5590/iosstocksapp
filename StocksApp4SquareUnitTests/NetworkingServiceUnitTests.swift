//


import XCTest
@testable import StocksApp4Square

class NetworkingManagerUnitTests: XCTestCase {

    var networkingManager: NetworkingServiceProtocol?
    
    override func setUpWithError() throws {
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [MockURLProtocol.self]
        let urlSession = URLSession(configuration: config)
        
        networkingManager = NetworkingService(session: urlSession)
    }

    override func tearDownWithError() throws {
        MockURLProtocol.stubResponseData = nil
        MockURLProtocol.error = nil
        networkingManager = nil
    }

    func testGET_WhenAPIReturnsStocks_ResponseShouldHaveMoreThen0Stocks() throws {
        // Arrange
        MockURLProtocol.stubResponseData = MockResponses.successStocksApiResponse.data(using: .utf8)
        MockURLProtocol.error = nil
        
        let expectation = expectation(description: "TestExpectation")
        
        // Act
        do {
            try networkingManager?.GET(type: PortfolioAPIResponse.self, urlString: "www.dummyURL.com", completion: { result in
                switch(result) {
                    
                case .success(let response):
                    // Assert
                    XCTAssert(response.stocks.count > 0, "Response should be not empty, but is empty")
                case .failure(let error):
                    XCTFail("Should not have an error, but it has an error: \(error.localizedDescription)")
                }
                expectation.fulfill()
            })
            wait(for: [expectation], timeout: 2)
        } catch  {
            XCTFail("Should not have thrown an error, but it threw error: \(error.localizedDescription)")
        }
    }
    
    func testGET_WhenAPIReturnsEmptyStocks_StocksReturnedShouldBe0() throws {
        // Arrange
        MockURLProtocol.stubResponseData = MockResponses.emptyStocksAPIResponse.data(using: .utf8)
        MockURLProtocol.error = nil
        
        let expectation = expectation(description: "TestExpectation")
        do {
            //Act
            try networkingManager?.GET(type: PortfolioAPIResponse.self, urlString: "www.dummyURL.com", completion: { result in
                switch(result) {
                    
                case .success(let response):
                    // Assert
                    XCTAssert(response.stocks.count == 0, "Response should be not empty, but is empty")
                case .failure(let error):
                    XCTFail("Should not have an error, but it has an error: \(error.localizedDescription)")
                }
                expectation.fulfill()
            })
            wait(for: [expectation], timeout: 2)
        } catch  {
            XCTFail("Should not have thrown an error, but it threw error: \(error.localizedDescription)")
        }
    }
    
    func testGET_WhenAPIReturnsMalformedResponse_GETShouldReturnAFailureErrorOfFailedToDecode() throws {
        // Arrange
        MockURLProtocol.stubResponseData = MockResponses.malformedStocksApiResponse.data(using: .utf8)
        MockURLProtocol.error = nil
        
        let expectation = expectation(description: "TestExpectation")
        
        do {
            // Act
            try networkingManager?.GET(type: PortfolioAPIResponse.self, urlString: "www.dummyURL.com", completion: { result in
                switch(result) {
                    
                // Assert
                case .success(_):
                    XCTFail("Should not return a valid response, but returned a response")
                case .failure(let error):
                    XCTAssert(error.localizedDescription == APIError.failedToDecode.localizedDescription, "Should return failed to decode error, but returned: \(error.localizedDescription)")
                }
                expectation.fulfill()
            })
            wait(for: [expectation], timeout: 2)
        } catch  {
            XCTFail("Should not have thrown an error, but it threw error: \(error.localizedDescription)")
        }
    }
    
    func testGET_WhenAPIReturnsError_ShouldReturnAFailureErrorOfSomeWentWrong() throws {
        // Arrange
        MockURLProtocol.stubResponseData = nil
        MockURLProtocol.error = APIError.somethingWentWrong
        
        let expectation = expectation(description: "TestExpectation")
        do {
            // Act
            try networkingManager?.GET(type: PortfolioAPIResponse.self, urlString: "www.dummyURL.com", completion: { result in
                switch(result) {
                
                // Assert
                case .success(_):
                    XCTFail("Should not return a valid response, but returned a response")
                case .failure(let error):
                    XCTAssert(error.localizedDescription == APIError.somethingWentWrong.localizedDescription, "Should return somethingWentWrong error, but returned: \(error.localizedDescription)")
                }
                expectation.fulfill()
            })
            wait(for: [expectation], timeout: 2)
        } catch  {
            XCTFail("Should not have thrown an error, but it threw error: \(error.localizedDescription)")
        }
    }
    
    func testGET_WhenURLInvalid_ShouldInvalidUrlError() throws {
        do {
            // Act
            try networkingManager?.GET(type: PortfolioAPIResponse.self, urlString: "This is an invalid URL", completion: { result in
                switch(result) {
                
                // Assert
                case .success(_):
                    XCTFail("Should not return a valid response, but returned a response")
                case .failure(let error):
                    XCTFail("Should not return aerror, but returned \(error)")
                }
            })
        } catch {
            XCTAssert((error as! APIError).localizedDescription == APIError.invalidURL.localizedDescription)
        }
    }
}
