# CashApp Coding CHallege

## Time spent on Project

5-5.5 hours.

### How to Run the project

There are no external libraries of dependency managers are being used. So we can simply run the app by opening it in `XCode 13`. For convenience of testing different endpoints provided, I have created a simple AlertSheet that can be used to switch between different URLs to test how the app handles different states such as Empty Stocks list, Malformed response, Error state etc.

<img src="Demo.gif">

## Architecture of the App

To build the application MVVM pattern with Repository and Services was used. The decision to use this architecture was taken based on my previous experience with this architecture that allows ease of dividing responsibility of different operations throughout the app aka Single Responsibility Principle and Unit Testing.

-   **Models:** Define the data of the application that corresponds to an API or Database.
-   **Views(Views and ViewControllers):** UI screens and components of the application.
-   **ViewModels:** Modifies model into data that gets displayed on the UI and methods that fired from the UI to perform operations.
-   **Repositories:** Defines data operations such as `loadStocks` in this case, but in general all types of CRUD operations.
-   **Services:** Classes that deal with actual code of calling external resources such as an API or a Database.

Also Protocol oriented approach was taken to define the contract which makes it easy to create Mocks during Unit Testing.

### Focus and Trade offs

**Focus:**
Main focus for the project was to structure the code that is easy to understand, extend and test & every component has a specific purpose. Data flow is simple to understand, and mostly goes one way at a single time.

**Trade offs**
Avoided creating Bindings in the project between ViewController and ViewModel to keep the project structure simple and testing simpler.

Usually I would another layer between a ViewModel and a Repository called Interactors that will actually hold the business logic of the app, so the responsibility of the ViewModel is only to convert the data returned by the Interactor into an representable form for the View. But in this case if we decide the extend the app which adds some business logic, it will be added in the ViewModel layer making it a little more complex. But for the sake of simplicity and to avoid another layer to Unit Test I excluded the Interactors.

### Copied Code

The only copied code in this project is `NetworkingService` class, which I copied from my other project. It is the boilerplate code to make a networking call to an API that I use most often in my personal projects. I copied and simplified for this project like removing the code that added header fields, or query parameters to build `URLRequest`. It is simply using `URLSession` class to make a `GET` request.
